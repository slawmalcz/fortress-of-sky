﻿using ControllPosition;
using UnityEngine;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        public float speed = 1;
        public float rotationSpeed = 1;
        public InteractingZone interactiveObject = null;
        public Camera mainCamera;
        public void Update()
        {
            //Debug.Log($"Input Axis: ({Input.GetAxis("Vertical")},{Input.GetAxis("Horizontal")})");
            GetComponent<Rigidbody>().velocity = gameObject.transform.forward * speed * Input.GetAxis("Vertical");
            gameObject.transform.eulerAngles =new Vector3(
                gameObject.transform.eulerAngles.x, 
                gameObject.transform.eulerAngles.y + Input.GetAxis("Horizontal") * rotationSpeed,
                gameObject.transform.eulerAngles.z);

            if (Input.GetKey(KeyCode.F) && interactiveObject != null)
                interactiveObject.Interact(this);

        }
    }
}
