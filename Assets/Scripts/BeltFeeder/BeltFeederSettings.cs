﻿using UnityEngine;

namespace BeltFeeder
{
    /// <summary>
    /// Settings for functioning of BeltFeedee in plane
    /// </summary>
    [CreateAssetMenu(fileName = "BeltSettings", menuName = "ScriptableObjects/BeltFeederSettings", order = 1)]
    public class BeltFeederSettings : ScriptableObject
    {
        /// <summary>
        /// Model of bullet and casing for display during transition
        /// </summary>
        public GameObject bulletModel = null;
        /// <summary>
        /// Defines speed of traversing belt in percent per second.
        /// </summary>
        [Tooltip("Percent progress per second")]
        public float transferSpeed = 0.01f;
        /// <summary>
        /// Defines rate of spawning bullets at beggining of belts
        /// </summary>
        [Tooltip("Number of bullets spawn per second")]
        public float spawnRate = 1;

        /// <summary>
        /// Calculates how much transfer is achived in given time
        /// </summary>
        /// <param name="deltaTime">Time of incrementation</param>
        /// <returns>Percent of incrementation in traversing belt</returns>
        public float PositionIncrementation(float deltaTime) => transferSpeed * deltaTime;
    }
}
