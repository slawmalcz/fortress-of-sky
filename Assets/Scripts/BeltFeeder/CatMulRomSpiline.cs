﻿using System.Collections.Generic;
using UnityEngine;

namespace BeltFeeder
{
    public class CatMulRomSpiline
	{
		// Use the transforms of GameObjects in 3d space as your points or define array with desired points
		private Transform[] points;

		// Store points on the Catmull curve so we can visualize them
		List<Vector3> newPositions
			= new List<Vector3>();
		List<Quaternion> newRotations = new List<Quaternion>();

		// How many points you want on the curve
		uint resolution = 10;

		// Parametric constant: 0.0 for the uniform spline, 0.5 for the centripetal spline, 1.0 for the chordal spline
		public float alpha = 0.5f;

		/////////////////////////////
		public CatMulRomSpiline(Transform[] points) : this(points, (uint)(points.Length * 4)) { }

		public CatMulRomSpiline(Transform[] points, uint resolution)
		{
			this.points = points;
			this.resolution = resolution;
			GeneratePositionPoints();
			GenerateRotationPoints();
		}

		public void GetTransform(float t, out Vector3 position, out Quaternion rotation)
        {
			var LNumber = Mathf.FloorToInt(resolution * t);
			var rest = resolution * t - LNumber;
			position = Vector3.Lerp(newPositions[LNumber], newPositions[(LNumber + 1 > newPositions.Count)?LNumber:LNumber+1], rest);
			rotation = Quaternion.Lerp(newRotations[LNumber], newRotations[(LNumber + 1 > newPositions.Count)? LNumber : LNumber + 1], rest);
		}

		void GeneratePositionPoints()
		{
			newPositions.Clear();

			Vector3 p0 = points[0].position; // Vector3 has an implicit conversion to Vector2
			Vector3 p1 = points[1].position;
			Vector3 p2 = points[2].position;
			Vector3 p3 = points[3].position;

			float t0 = 0.0f;
			float t1 = GetT(t0, p0, p1);
			float t2 = GetT(t1, p1, p2);
			float t3 = GetT(t2, p2, p3);

			for (float t = t1; t < t2; t += ((t2 - t1) / (float)resolution))
			{
				Vector3 A1 = (t1 - t) / (t1 - t0) * p0 + (t - t0) / (t1 - t0) * p1;
				Vector3 A2 = (t2 - t) / (t2 - t1) * p1 + (t - t1) / (t2 - t1) * p2;
				Vector3 A3 = (t3 - t) / (t3 - t2) * p2 + (t - t2) / (t3 - t2) * p3;

				Vector3 B1 = (t2 - t) / (t2 - t0) * A1 + (t - t0) / (t2 - t0) * A2;
				Vector3 B2 = (t3 - t) / (t3 - t1) * A2 + (t - t1) / (t3 - t1) * A3;

				Vector3 C = (t2 - t) / (t2 - t1) * B1 + (t - t1) / (t2 - t1) * B2;

				newPositions.Add(C);
			}
		}
		void GenerateRotationPoints()
		{
			newRotations.Clear();

			Quaternion p0 = points[0].rotation; // Vector3 has an implicit conversion to Vector2
			Quaternion p1 = points[1].rotation;
			Quaternion p2 = points[2].rotation;
			Quaternion p3 = points[3].rotation;

			float t0 = 0.0f;
			float t1 = GetT(t0, p0, p1);
			float t2 = GetT(t1, p1, p2);
			float t3 = GetT(t2, p2, p3);

			for (float t = t1; t < t2; t += ((t2 - t1) / (float)resolution))
			{
				Quaternion A1 = Add(Multiply((t1 - t) / (t1 - t0), p0), Multiply((t - t0) / (t1 - t0), p1));
				Quaternion A2 = Add(Multiply((t2 - t) / (t2 - t1), p1), Multiply((t - t1) / (t2 - t1), p2));
				Quaternion A3 = Add(Multiply((t3 - t) / (t3 - t2), p2), Multiply((t - t2) / (t3 - t2), p3));
				Quaternion B1 = Add(Multiply((t2 - t) / (t2 - t0), A1), Multiply((t - t0) / (t2 - t0), A2));
				Quaternion B2 = Add(Multiply((t3 - t) / (t3 - t1), A2), Multiply((t - t1) / (t3 - t1), A3));
				Quaternion C  = Add(Multiply((t2 - t) / (t2 - t1), B1), Multiply((t - t1) / (t2 - t1), B2));

				newRotations.Add(C);
			}
		}

		float GetT(float t, Vector3 p0, Vector3 p1)
		{
			float a = Mathf.Pow((p1.x - p0.x), 2.0f) + Mathf.Pow((p1.y - p0.y), 2.0f) + +Mathf.Pow((p1.z - p0.z), 2.0f);
			float b = Mathf.Pow(a, alpha * 0.5f);

			return (b + t);
		}
		float GetT(float t, Quaternion p0, Quaternion p1)
		{
			float a = Mathf.Pow((p1.x - p0.x), 2.0f) + Mathf.Pow((p1.y - p0.y), 2.0f) + Mathf.Pow((p1.z - p0.z), 2.0f) + Mathf.Pow((p1.w - p0.w), 2.0f);
			float b = Mathf.Pow(a, alpha * 0.5f);

			return (b + t);
		}

		Quaternion Multiply(float value,Quaternion quaternion)
        {
			var x = quaternion.x * value;
			var y = quaternion.y * value;
			var z = quaternion.z * value;
			var w = quaternion.w * value;
			return new Quaternion(x, y, z,w);
		}
        Quaternion Add(Quaternion a, Quaternion b) => new Quaternion(
                a.x + b.x,
                a.y + b.y,
                a.z + b.z,
                a.w + b.w);
    }
}
