﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.BeltFeeder
{
    class AutoConnectJoint : MonoBehaviour
    {
        void Awake()
        {
            GetComponent<HingeJoint>().connectedBody = transform.parent.GetComponent<Rigidbody>();
        }
    }
}
