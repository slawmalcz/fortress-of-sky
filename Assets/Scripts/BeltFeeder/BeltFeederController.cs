﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace BeltFeeder
{
    public class BeltFeederController : MonoBehaviour
    {
        public GameObject staticBeltFeeder = null;
        public GameObject dynamicBeltFeeder = null;
        public GameObject dynamicBeltFeederEnd = null;

        public BeltFeederSpawnerController spawnerController = null;

        private BeltFeederState _state = BeltFeederState.Static;
        public BeltFeederState State {
            get => _state;
            set {
                switch (value)
                {
                    case (BeltFeederState.Static):
                        ActivateStaticBeltFeeder();
                        break;
                    case (BeltFeederState.Dynamic):
                        ActivateDynamicBeltFeeder();
                        break;
                }
                _state = value;
            }
        }

        private void ActivateStaticBeltFeeder()
        {
            for (var i = 0; i < spawnerController.bulletDump.childCount; i++)
                Destroy(spawnerController.bulletDump.GetChild(i).gameObject);
            dynamicBeltFeeder.SetActive(false);
            dynamicBeltFeederEnd.SetActive(false);
            staticBeltFeeder.SetActive(true);
        }
        private void ActivateDynamicBeltFeeder()
        {
            dynamicBeltFeeder.SetActive(true);
            dynamicBeltFeederEnd.SetActive(true);
            staticBeltFeeder.SetActive(false);
        }

        public void StartFire()
        {
            if (State == BeltFeederState.Dynamic)
                spawnerController.isActive = true;
        }
        public void StopFire()
        {
            if (State == BeltFeederState.Dynamic)
                spawnerController.isActive = false;
        }
    }

    public enum BeltFeederState
    {
        Static,
        Dynamic
    }
}
