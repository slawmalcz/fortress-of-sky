﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BeltFeeder
{
    /// <summary>
    /// Main controller for belt feeder. Is responsible for:
    /// * spawning bullets
    /// * changing transform of bullets
    /// * despawning bullets
    /// </summary>
    public class BeltFeederSpawnerController : MonoBehaviour
    {
        [Header("Settings")]
        public BeltFeederSettings settings = null;
        [Header("Operation")]
        public bool isActive = false;

        public GameObject beltStart = null;
        public GameObject beltEnd = null;

        public Transform bulletDump = null;


        private List<BulletInstance> activeBullets = new List<BulletInstance>();
        private List<Transform> pointsList = new List<Transform>();

        public void Start()
        {
            var currentElement = beltStart.transform;
            while (currentElement.childCount > 0)
            {
                pointsList.Add(currentElement);
                currentElement = currentElement.GetChild(0);
            }
            pointsList.Add(currentElement);
            pointsList.Add(beltEnd.transform);
        }

        private float timeSinceLastSpawn = 0;

        public void Update()
        {
            if (!isActive)
                return;
            //Spawn bullets
            timeSinceLastSpawn += Time.deltaTime;
            if (timeSinceLastSpawn > settings.spawnRate)
            {
                var newBullet = new BulletInstance(Instantiate(settings.bulletModel), pointsList, settings.transferSpeed);
                newBullet.instance.transform.parent = bulletDump;
                newBullet.instance.transform.position = beltStart.transform.position;
                newBullet.instance.transform.rotation = beltStart.transform.rotation;
                activeBullets.Add(newBullet);
                timeSinceLastSpawn = 0;
            }
            //Check for nulls
            activeBullets = activeBullets.Where(x => x.instance != null).ToList();
            //Calculate new positions
            for (var i = 0; i < activeBullets.Count; i++)
                activeBullets[i].CalculateNewTransform(Time.deltaTime);
        }

        private class BulletInstance
        {
            public GameObject instance = null;
            private static List<Transform> pointsList = null;
            private static float speed = 0;
            private int myNextPosition = 0;

            public BulletInstance(GameObject instance, List<Transform> pointsList, float speed)
            {
                this.instance = instance;
                if (BulletInstance.pointsList == null)
                    BulletInstance.pointsList = pointsList;
                if (BulletInstance.speed != speed)
                    BulletInstance.speed = speed;
            }

            public void CalculateNewTransform(float deltaTime)
            {
                var myCurrentPosition = instance.transform.position;
                var distanceToCover = speed * deltaTime;
                while (distanceToCover > Vector3.Distance(myCurrentPosition, pointsList[myNextPosition].position))
                {
                    if(myNextPosition + 1 >= pointsList.Count)
                    {
                        instance.transform.position = pointsList[myNextPosition].position;
                        instance.transform.rotation = pointsList[myNextPosition].rotation;
                        Destroy(instance);
                        return;
                    }
                    myNextPosition++;
                }
                var t = distanceToCover / Vector3.Distance(myCurrentPosition, pointsList[myNextPosition].position);
                instance.transform.position = Vector3.Lerp(instance.transform.position, pointsList[myNextPosition].position, t);
                instance.transform.rotation = Quaternion.Lerp(instance.transform.rotation, pointsList[myNextPosition].rotation, t);
            }
        }
    }
}
