﻿using System.Collections.Generic;
using UnityEngine;

namespace GunControl
{
    public class HitInfoController : MonoBehaviour
    {
        private struct TimedObject
        {
            public GameObject gameObject;
            public float timeLeft;

            public TimedObject(GameObject gameObject, float timeLeft) : this()
            {
                this.gameObject = gameObject;
                this.timeLeft = timeLeft;
            }
        }

        private List<TimedObject> hitList = new List<TimedObject>();

        public void AddElement(GameObject element) => 
            hitList.Add(new TimedObject(element, 20));

        public void RemoveAllFromPosition(Vector3 position, float clerance) => 
            hitList.RemoveAll(x => Vector3.Distance(x.gameObject.transform.position, position) < clerance);

        public void Update()
        {
            var listToRemove = new List<TimedObject>();
            for(var i = 0; i < hitList.Count; i++)
            {
                var currentElement = hitList[i];
                currentElement.timeLeft -= Time.deltaTime;
                if (currentElement.timeLeft < 0)
                {
                    Destroy(currentElement.gameObject);
                    listToRemove.Add(currentElement);
                }
            }
            hitList.RemoveAll(x=>listToRemove.Contains(x));
        }
    }
}
