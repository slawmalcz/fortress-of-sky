﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GunControl
{
    public class GunController : MonoBehaviour
    {
        [Header("Prefabs")]
        public GameObject bullet;
        public GameObject barrel;
        public ShootSFX shootSFX;
        public GameObject impactParticle;
        [Header("Settings")]
        public GunSettings gunCharacteristics;
        public BulletSettings bulletCharacteristics;

        private float timeSinceLastShoot = 0;
        private float TimeBetweanShots;
        public bool trigger = false;

        public void Start()
        {
            TimeBetweanShots = 1 / (gunCharacteristics.fireRate / 60);
            shootSFX.Shoot(() => trigger);
        }
        public void Update()
        {

            timeSinceLastShoot += Time.deltaTime;
            if (trigger)
            {
                if (timeSinceLastShoot >= TimeBetweanShots)
                {
                    SpawnParticle();
                    SpawnBullets();
                    timeSinceLastShoot = 0;
                }
            }
        }

        private void SpawnParticle()
        {

        }

        private void SpawnBullets()
        {
            //PhysicalBullet
            var bulletInstance = Instantiate(bullet, barrel.transform);
            bulletInstance.transform.localPosition = new Vector3(0, 0, 0);
            bulletInstance.transform.parent = null;
            var bulletRigidBody = bulletInstance.GetComponent<Rigidbody>();
            bulletRigidBody.velocity = barrel.transform.forward * gunCharacteristics.fireRate;
            Destroy(bulletInstance, gunCharacteristics.maxRange / bulletCharacteristics.initialBulletVelocity);
            //RayCast bullet
            var shootDirection = (barrel.transform.forward + new Vector3(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value) * gunCharacteristics.dispersionPower).normalized;
            var shootRay = new Ray(barrel.transform.position, shootDirection);
            if (Physics.Raycast(shootRay, out var contactInfo, gunCharacteristics.maxRange))
            {
                //SpawnHit Particles
                var hitInfoController = contactInfo.collider.gameObject.GetComponentInChildren<HitInfoController>();
                if (hitInfoController is null)
                {
                    var hitInfoControllerGO = new GameObject("TargetHits");
                    hitInfoControllerGO.transform.parent = contactInfo.collider.transform;
                    hitInfoControllerGO.transform.position = Vector3.zero;
                    hitInfoController = hitInfoControllerGO.AddComponent<HitInfoController>();
                }
                else
                {
                    hitInfoController.RemoveAllFromPosition(contactInfo.point, bulletCharacteristics.impactCraterSize);
                }
                var impact = Instantiate(impactParticle, hitInfoController.transform);
                impact.transform.position = contactInfo.point + shootDirection * -0.01f;
                impact.transform.localScale = Vector3.one * bulletCharacteristics.impactCraterSize;
                //Give impact force
                var hitedRigidbody = contactInfo.collider.gameObject.GetComponent<Rigidbody>();
                if (hitedRigidbody != null)
                {
                    var bulletForce = shootDirection * bulletCharacteristics.GetBulletForce(contactInfo.distance);
                    hitedRigidbody.AddForceAtPosition(bulletForce, contactInfo.point, ForceMode.Impulse);
                }
            }
        }
    }
}