﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSightController : MonoBehaviour
{
    public float sightDistance = 100;
    private const float HIGHT_OF_SIGHT = 0.646f;


    /// <summary>
    /// Using in sights for mimicing guns rotation
    /// </summary>

    public Transform target;
    void Update()
    {
        var differenceAngle = Mathf.Atan(sightDistance / HIGHT_OF_SIGHT);
        var newRotation = new Vector3(target.eulerAngles.x - differenceAngle, target.eulerAngles.y, target.eulerAngles.z);
        transform.eulerAngles = newRotation;
    }
}
