﻿using System;
using System.Collections;
using UnityEngine;

namespace GunControl
{
    public class ShootSFX : MonoBehaviour
    {
        public AudioSource emmiter;

        public AudioClip shootStart;
        public AudioClip shootLoop;
        public AudioClip shootEnd;

        private Coroutine currentCoroutine = null;

        public void Shoot(Func<bool> isShootingHandler)
        {
            if (currentCoroutine != null)
                return;
            else
                currentCoroutine = StartCoroutine(Shooting(isShootingHandler));
        }

        private IEnumerator Shooting(Func<bool> isShootingHandler)
        {
            emmiter.Stop();
            emmiter.loop = false;
            emmiter.clip = shootStart;
            emmiter.Play();
            yield return new WaitWhile(() => { return emmiter.isPlaying && isShootingHandler.Invoke(); });
            if (!isShootingHandler.Invoke())
            {
                emmiter.Stop();
                emmiter.loop = false;
                emmiter.clip = shootEnd;
                emmiter.Play();
                currentCoroutine = null;
            }
            else
            {
                emmiter.Stop();
                emmiter.clip = shootLoop;
                emmiter.loop = true;
                emmiter.Play();
                yield return new WaitWhile(isShootingHandler);
                emmiter.Stop();
                emmiter.loop = false;
                emmiter.clip = shootEnd;
                emmiter.Play();
                currentCoroutine = null;
            }
        }
    }
}
