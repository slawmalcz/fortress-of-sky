﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GunControl
{
    [CreateAssetMenu(fileName = "GunSettings", menuName = "GunSO/GunCharacteristics", order = 1)]
    public class GunSettings : ScriptableObject
    {
        public float fireRate = 600;
        public float maxRange = 4000;
        public float dispersionPower = 0.01f;
    }
}
