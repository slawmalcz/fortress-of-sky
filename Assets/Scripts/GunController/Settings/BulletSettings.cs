﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GunControl
{
    [CreateAssetMenu(fileName = "BulletSettings", menuName = "GunSO/BulletCharacteristics", order = 1)]
    public class BulletSettings : ScriptableObject
    {
        public float bulletMass = 0.23f;
        public float initialBulletVelocity = 890;
        public float bulletForceDropCoeficient = 0.1f;
        public float impactCraterSize = 1;

        public float GetBulletForce(float distance) => Mathf.Clamp((bulletMass * initialBulletVelocity) - (distance * bulletForceDropCoeficient), 0f, float.PositiveInfinity);
    }
}
