﻿using BeltFeeder;
using GunControl;
using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ControllPosition
{
    public class TailTurretControllPosition : MonoBehaviour,IInteractiveObject
    {
        public Transform mainPositionCameraSlot = null;
        public BeltFeederController leftBeltFeeder = null;
        public BeltFeederController rightBeltFeeder = null;
        public GunController[] guns = null;

        [Header("Rotation Units")]
        public Transform verticalRotationUnit = null;
        public Transform horisontalRotationUnit = null;

        [Header("Controll Settings")]
        [Range(0, 90)]
        public float maxHorisontalTilt = 30;
        [Range(0, 90)]
        public float maxVerticalTilt = 30;


        public bool IsOccupied { get; private set; } = false;

        public void GetIn(Camera mainCamera)
        {
            //Camera assignation
            mainCamera.transform.parent = mainPositionCameraSlot;
            mainCamera.transform.localPosition = Vector3.zero;
            mainCamera.transform.localRotation = Quaternion.identity;
            //BeltFeeders swap
            leftBeltFeeder.State = BeltFeederState.Dynamic;
            rightBeltFeeder.State = BeltFeederState.Dynamic;
            //Set internal values
            IsOccupied = true;
        }

        public Camera GetOut()
        {
            var mainCamera = mainPositionCameraSlot.GetChild(0);
            mainCamera.transform.parent = null;
            mainCamera.transform.position = Vector3.zero;
            mainCamera.transform.rotation = Quaternion.identity;

            verticalRotationUnit.transform.localRotation = Quaternion.identity;
            horisontalRotationUnit.transform.localRotation = Quaternion.identity;

            leftBeltFeeder.State = BeltFeederState.Static;
            rightBeltFeeder.State = BeltFeederState.Static;

            IsOccupied = false;

            return mainCamera.GetComponent<Camera>();
        }

        public void Update()
        {
            if (!IsOccupied)
                return;

            //Mouse controll
            var mouseCoord = ((Input.mousePosition / new Vector2(Screen.width, Screen.height)) - new Vector2(0.5f,0.5f)) * 2;
            var outputRotations = mouseCoord * new Vector2(maxHorisontalTilt, maxVerticalTilt);

            horisontalRotationUnit.localEulerAngles = new Vector3(
                horisontalRotationUnit.localEulerAngles.x,
                outputRotations.x,
                horisontalRotationUnit.localEulerAngles.z);
            verticalRotationUnit.localEulerAngles = new Vector3(
                outputRotations.y,
                verticalRotationUnit.localEulerAngles.y,
                verticalRotationUnit.localEulerAngles.z);
            //Fire
            if (Input.GetMouseButton(0))
            {
                leftBeltFeeder.StartFire();
                rightBeltFeeder.StartFire();
                for (var i = 0; i < guns.Length; i++)
                    guns[i].trigger = true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                leftBeltFeeder.StopFire();
                rightBeltFeeder.StopFire();
                for (var i = 0; i < guns.Length; i++)
                    guns[i].trigger = false;
            }

            if (Input.GetKeyDown(KeyCode.F))
                GetOut();
        }

        public void Use(PlayerController player)
        {
            GetIn(player.mainCamera);
        }
    }

}

