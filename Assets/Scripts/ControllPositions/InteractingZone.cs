﻿using Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace ControllPosition
{
    public class InteractingZone : MonoBehaviour
    {
        public Collider interactionZone;

        public GameObject attachedObject = null;
        private IInteractiveObject AttachedObject => attachedObject.GetComponent<IInteractiveObject>();

        public void Start()
        {
            interactionZone.isTrigger = true;
        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<PlayerController>())
                other.GetComponent<PlayerController>().interactiveObject = this;
        }

        public void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<PlayerController>() && other.GetComponent<PlayerController>().interactiveObject == this)
                other.GetComponent<PlayerController>().interactiveObject = null;
        }

        public void Interact(PlayerController characterController)
        {
            AttachedObject.Use(characterController);
        }
    }
}
