﻿using Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControllPosition
{
    public interface IInteractiveObject
    {
        void Use(PlayerController player);
    }
}
