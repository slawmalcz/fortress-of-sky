﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitEffectVFXController : MonoBehaviour
{
    public float decalLifeTime = 20f;
    public GameObject hitEffect = null;
    public GameObject decalPrefab = null;

    public void Start()
    {
        decalPrefab.transform.eulerAngles = new Vector3(Random.Range(-180, 180), decalPrefab.transform.eulerAngles.y, decalPrefab.transform.eulerAngles.z);
        decalPrefab.SetActive(true);
        StartCoroutine(DeactivateParticles(5));
    }

    private IEnumerator DeactivateParticles(float time)
    {
        yield return new WaitForSeconds(time);
        hitEffect.SetActive(false);
    }
}
